#!/bin/bash

# wttr.in by ssh client IP

DEFAULT_URL="wttr.in"
DEFAULT_LOCATION="" #Change if needed; if empty will display your IP location
DEFAULT_ARGS="" #Change if needed

if [ -n "$SSH_CONNECTION" ]; then
CLIENT_LOCATION=$(echo $SSH_CLIENT | awk '{print $1}')

curl "$DEFAULT_URL/@$CLIENT_LOCATION?$ARGS"
fi

curl "$DEFAULT_URL/$DEFAULT_LOCATION?$DEFAULT_ARGS"